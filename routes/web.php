<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'GuestController@index')->name('home');
Route::get('/sell', 'HomeController@viewSell')->name('sell');
Route::post('/sell', 'HomeController@postSell')->name('sell.post');
Route::get('/car/{licence_id}', 'GuestController@carDetail')->name('car.detail');
Route::post('/car/edit', 'HomeController@carEdit')->name('car.edit');
Route::post('/car/delete', 'HomeController@carDelete')->name('car.delete');
Route::post('/car/status/sell', 'HomeController@carStatusSell')->name('car.status.sell');
Route::post('/car/status/unsell', 'HomeController@carStatusUnsell')->name('car.status.unsell');
// Route::get('/car/detail', 'GuestController@carDetail')->name('car.detail');
Route::get('/my/car', 'HomeController@myCar')->name('my.car');
Route::get('/payment/history', 'HomeController@paymentHistory')->name('payment.history');
Route::post('/payment/delete', 'HomeController@paymentDelete')->name('payment.delete');
