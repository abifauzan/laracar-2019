@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top:50px;margin-bottom:50px;">

        <div class="container">

            <!-- Portfolio Item Heading -->
            <h1 >Car Detail</h1>

            <!-- Portfolio Item Row -->
            <div class="row">

                <div class="col-md-8">
                    <img class="img-fluid" src="http://placehold.it/750x500" alt="">
                </div>

                <div class="col-md-4">
                    <h3 class="my-3">Car Name</h3>
                    <h3 class="my-3">Price : Rp <strong>{{number_format(50000000)}}</strong></h3><br>

                    <table class="table table-striped">
                        <tr>
                            <th style="width:40%">Car Licence</th>
                            <td class="text-uppercase">A 8 AY</td>
                        </tr>
                        <tr>
                            <th style="width:40%">Type</th>
                            <td>Hatchback</td>
                        </tr>
                        <tr>
                            <th style="width:40%">Brand</th>
                            <td>Brand Name</td>
                        </tr>
                        <tr>
                            <th style="width:40%">CC</th>
                            <td>1000 CC</td>
                        </tr>
                        <tr>
                            <th style="width:40%">Color</th>
                            <td>Blue</td>
                        </tr>
                    </table>
                    <button class="btn btn-primary btn-lg">BUY THIS CAR</button>
                </div>

            </div>
                    <!-- /.row -->


        </div>

    </div>
@endsection
