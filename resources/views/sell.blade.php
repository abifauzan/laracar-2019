@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top:50px;margin-bottom:50px;">

        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-primary text-white">
                    Sell Your Car
                </div>
                <div class="card-body">
                    <form action="{{route('sell.post')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Car Licence</label>
                            <div class="col-sm-10">
                                <input type="text" name="car_licence" class="form-control" maxlength="8" placeholder="Input Car Licence" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Type</label>
                            <div class="col-sm-10">
                                <select name="id_type" class="form-control" required>
                                    <option value="" selected disabled> -- Select Type --</option>
                                @foreach($carType as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Year</label>
                            <div class="col-sm-10">
                                <input type="text" name="year" class="form-control" maxlength="4" placeholder="Input Year" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Brand</label>
                            <div class="col-sm-10">
                                <input type="text" name="brand" class="form-control" placeholder="Input Brand" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">CC</label>
                            <div class="col-sm-10">
                                <input type="text" name="cc" class="form-control" placeholder="Input CC" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Color</label>
                            <div class="col-sm-10">
                                <input type="text" name="color" class="form-control" placeholder="Input Color" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">Price</label>
                            <div class="col-sm-10">
                                <input type="text" name="price" placeholder="Input Price, Example : 50000000" class="form-control" required>
                                <span class="inline-block">*only number</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-2 col-form-label">&nbsp;</label>
                            <div class="col-sm-10">
                                <button type="submit" class="btn btn-primary">Submit</button>&nbsp;
                                <button type="reset" class="btn btn-warning">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
