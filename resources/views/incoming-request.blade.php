@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top:50px;margin-bottom:50px;">

        <div class="container">

            <h1 >Incoming Request</h1>
            <br>
            @if(count($datas) == 0)
                <div class="alert alert-warning">
                    <span>You don't have any request</span>
                </div>
            @endif
            <br>
            @if(count($datas) > 0)
                <table class="table table-striped table-hover">
                    <thead>
                        <th>No</th>
                        <th>Invoice</th>
                        <th>Car Licence</th>
                        <th>Price</th>
                        <th>Transaction Date</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @php($no=0)
                        @foreach($datas as $data)
                            @php($no++)
                            <tr>
                                <td>{{$no}}</td>
                                <td>{{$data->invoice_id}}</td>
                                <td>{{$data->car_licence}}</td>
                                <td>Rp {{number_format($data->price)}}</td>
                                <td>{{$data->created_at}}</td>
                                <td>
                                    <form action="{{route('request.confirm')}}" method="POST" style="display:inline-block">
                                        @csrf
                                        <input type="hidden" name="invoice_id" value="{{$data->invoice_id}}">
                                        <button type="submit" class="btn btn-success btn-sm">Confirm</button>&nbsp;
                                    </form>
                                    <form action="{{route('request.reject')}}" method="POST" style="display:inline-block">
                                            @csrf
                                            <input type="hidden" name="invoice_id" value="{{$data->invoice_id}}">
                                            <button type="submit" class="btn btn-danger btn-sm">Reject</button>
                                        </form>

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        </div>

    </div>
@endsection
