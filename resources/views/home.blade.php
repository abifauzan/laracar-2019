@extends('layouts.app')

@section('content')
 <!-- Jumbotron Header -->
 <header class="jumbotron my-4">
        <h1 class="display-3">Web Programming</h1>
        <p class="lead">Developer : <b> Abi Fauzan </b><br>
            NIM : <b> 41516020002 </b></p>
        <p class="lead">Features : <br>
        - Login User <br>
        - Register User <br>
        - Sell Car <br>
        - View Car Collections <br>
        - Change status car to sell or owned (un-sell) <br>
        - Create Transaction Invoice <br>
        - Delete Invoice <br>
        - View Detail Invoice
        </p>
</header>


@endsection
