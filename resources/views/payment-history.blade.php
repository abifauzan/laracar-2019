@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top:50px;margin-bottom:50px;">

        <div class="container">

            <div class="col-lg-12">


                <h1>Transaction History</h1>
                <br>

                @if(count($dataSell) > 0)
                    <div class="col-lg-6 pull-left">
                        <div class="alert alert-info">Sale Transaction</div>

                        <table class="table table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th># Invoice</th>
                                <th>Car Licence</th>
                                <th>Price</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @php($no=0)
                                @foreach($dataSell as $data)
                                    @php($no++)
                                    @if($data->transaction_type == 'sell')
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$data->invoice_id}}</td>
                                            <td>{{$data->car_licence}}</td>
                                            <td>Rp. {{number_format($data->price)}}</td>
                                            {{-- <td>{{$data->year}}</td> --}}
                                            {{-- <td>Rp {{number_format($data->price)}}</td> --}}
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#detailTransSell{{$no}}" title="View Detail"><i class="fa fa-eye"></i></button>&nbsp;
                                                <form action="{{route('payment.delete')}}" method="POST" style="display:inline-block">
                                                    @csrf
                                                        <input type="hidden" name="invoice_id" value="{{$data->invoice_id}}">
                                                        <button class="btn btn-danger btn-sm" title="Remove" onclick="return confirm('Are You sure ?');"><i class="fa fa-trash"></i></button>
                                                </form>

                                            </td>
                                        </tr>
                                        <div id="detailTransSell{{$no}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-wrap">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label"> # Invoice ID</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->invoice_id}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Car Licence</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->car_licence}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Price</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="RP. {{number_format($data->price)}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Date Sell</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="{{$data->created_at}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                @endif

                @if(count($dataBuy) > 0)
                    <div class="col-lg-6 pull-left">
                        <div class="alert alert-info">Buy Transaction</div>

                        <table class="table table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th># Invoice</th>
                                <th>Car Licence</th>
                                <th>Price</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @php($no=0)
                                @foreach($dataBuy as $data)
                                    @php($no++)
                                    @if($data->transaction_type == 'buy')
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$data->invoice_id}}</td>
                                            <td>{{$data->car_licence}}</td>
                                            <td>Rp. {{number_format($data->price)}}</td>
                                            {{-- <td>{{$data->year}}</td> --}}
                                            {{-- <td>Rp {{number_format($data->price)}}</td> --}}
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#detailTransBuy{{$no}}" title="View Detail"><i class="fa fa-eye"></i></button>&nbsp;
                                                <form action="{{route('payment.delete')}}" method="POST" style="display:inline-block">
                                                    @csrf
                                                        <input type="hidden" name="invoice_id" value="{{$data->invoice_id}}">
                                                        <button class="btn btn-danger btn-sm" title="Remove" onclick="return confirm('Are You sure ?');"><i class="fa fa-trash"></i></button>
                                                </form>

                                            </td>
                                        </tr>
                                        <div id="detailTransBuy{{$no}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-wrap">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label"> # Invoice ID</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->invoice_id}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label"> Salesman</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->salesman->name}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Car Licence</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->car_licence}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Price</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="RP. {{number_format($data->price)}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Sales Commision</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="{{$data->salesman_commision}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Date Buy</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="{{$data->created_at}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>

                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                @endif
                <div class="clearfix"></div>
            </div>


        </div>

    </div>
@endsection
