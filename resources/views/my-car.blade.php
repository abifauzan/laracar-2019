@extends('layouts.app')

@section('content')
    <div class="row" style="margin-top:50px;margin-bottom:50px;">

        <div class="container">

            <div class="col-lg-12">


                <h1>My Car Collection</h1>
                <br>
                @if(count($datas) == 0)
                    <div class="alert alert-warning">
                        <span>You don't have any car collection</span>
                    </div>
                @endif

                @if(count($datas) > 0)
                    <div class="col-lg-6 pull-left">
                        <div class="alert alert-info">For Sale</div>

                        <table class="table table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th>Car Licence</th>
                                <th>Brand</th>
                                <th>Type</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @php($no=0)
                                @foreach($datas as $data)
                                    @php($no++)
                                    @if($data->status == 'sell')
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$data->car_licence}}</td>
                                            <td>{{$data->brand}}</td>
                                            <td>{{$data->type->name}}</td>
                                            {{-- <td>{{$data->year}}</td> --}}
                                            {{-- <td>Rp {{number_format($data->price)}}</td> --}}
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#detailSale{{$no}}" title="View Detail"><i class="fa fa-eye"></i></button>&nbsp;
                                                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#editSale{{$no}}" title="Edit"><i class="fa fa-pencil"></i></button>&nbsp;
                                                <form action="{{route('car.delete')}}" method="POST" style="display:inline-block">
                                                    @csrf
                                                        <input type="hidden" name="car_licence" value="{{$data->car_licence}}">
                                                        <button class="btn btn-danger btn-sm" title="Remove" onclick="return confirm('Are You sure ?');"><i class="fa fa-trash"></i></button>
                                                </form>

                                            </td>
                                        </tr>
                                        <div id="detailSale{{$no}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-wrap">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Car Licence</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->car_licence}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Type</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <select name="type" class="form-control" readonly>
                                                                            <option selected>{{$data->type->name}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Year</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->year}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Brand</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->brand}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">CC</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="{{$data->cc}} CC" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Color</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="{{$data->color}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Price</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="RP. {{number_format($data->price)}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label"></label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <form action="{{route('car.status.unsell')}}" method="POST">
                                                                            @csrf
                                                                            <input type="hidden" name="car_licence" value="{{$data->car_licence}}">
                                                                            <button class="btn btn-primary">UnSell this car</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <div id="editSale{{$no}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('car.edit')}}" method="POST">
                                                            @csrf
                                                            <div class="form-wrap">
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Car Licence</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="car_licence" class="form-control" value="{{$data->car_licence}}" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Type</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <select name="type" class="form-control" required>
                                                                            @foreach($carType as $type)
                                                                                <option value="{{$type->id}}" @if($data->id_type == $type->id) selected @endif>{{$type->name}}</option>
                                                                            @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Year</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" maxlength="4" name="year" class="form-control" value="{{$data->year}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Brand</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="brand" class="form-control" value="{{$data->brand}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">CC</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="cc" class="form-control" value="{{$data->cc}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Color</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="color" class="form-control" value="{{$data->color}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Price</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="price" class="form-control" value="{{$data->price}}" required>

                                                                        </div>
                                                                        <span class="inline-block">*number only</span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <div class="col-sm-offset-3 col-sm-9">
                                                                        <button type="submit" class="btn btn-success">Edit</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-6 pull-left">
                        <div class="alert alert-info">You owned these cars</div>

                        <table class="table table-striped table-hover">
                            <thead>
                                <th>No</th>
                                <th>Car Licence</th>
                                <th>Brand</th>
                                <th>Type</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @php($no=0)
                                @foreach($datas as $data)
                                    @php($no++)
                                    @if($data->status == 'owned')
                                        <tr>
                                            <td>{{$no}}</td>
                                            <td>{{$data->car_licence}}</td>
                                            <td>{{$data->brand}}</td>
                                            <td>{{$data->type->name}}</td>
                                            {{-- <td>{{$data->year}}</td> --}}
                                            {{-- <td>Rp {{number_format($data->price)}}</td> --}}
                                            <td>
                                                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#detailOwned{{$no}}" title="View Detail"><i class="fa fa-eye"></i></button>&nbsp;
                                                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#editOwned{{$no}}" title="Edit"><i class="fa fa-pencil"></i></button>&nbsp;
                                                <form action="{{route('car.delete')}}" method="POST" style="display:inline-block">
                                                    @csrf
                                                        <input type="hidden" name="car_licence" value="{{$data->car_licence}}">
                                                        <button class="btn btn-danger btn-sm" title="Remove" onclick="return confirm('Are You sure ?');"><i class="fa fa-trash"></i></button>
                                                </form>

                                            </td>
                                        </tr>
                                        <div id="detailOwned{{$no}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-wrap">
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Car Licence</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->car_licence}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Type</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <select name="type" class="form-control" readonly>
                                                                            <option selected>{{$data->type->name}}</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Year</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->year}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Brand</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" value="{{$data->brand}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">CC</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="{{$data->cc}} CC" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Color</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="{{$data->color}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label">Price</label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                            <input type="text" class="form-control" value="RP. {{number_format($data->price)}}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="" class="col-sm-3 control-label"></label>
                                                                <div class="col-sm-9">
                                                                    <div class="input-group">
                                                                        <form action="{{route('car.status.sell')}}" method="POST">
                                                                            @csrf
                                                                            <input type="hidden" name="car_licence" value="{{$data->car_licence}}">
                                                                            <button class="btn btn-primary">Sell this car</button>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        <div id="editOwned{{$no}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form action="{{route('car.edit')}}" method="POST">
                                                            @csrf
                                                            <div class="form-wrap">
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Car Licence</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="car_licence" class="form-control" value="{{$data->car_licence}}" readonly>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Type</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <select name="type" class="form-control" required>
                                                                            @foreach($carType as $type)
                                                                                <option value="{{$type->id}}" @if($data->id_type == $type->id) selected @endif>{{$type->name}}</option>
                                                                            @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Year</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" maxlength="4" name="year" class="form-control" value="{{$data->year}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Brand</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="brand" class="form-control" value="{{$data->brand}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">CC</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="cc" class="form-control" value="{{$data->cc}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Color</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="color" class="form-control" value="{{$data->color}}" required>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="" class="col-sm-3 control-label">Price</label>
                                                                    <div class="col-sm-9">
                                                                        <div class="input-group">
                                                                            <input type="text" name="price" class="form-control" value="{{$data->price}}" required>

                                                                        </div>
                                                                        <span class="inline-block">*number only</span>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group mb-0">
                                                                    <div class="col-sm-offset-3 col-sm-9">
                                                                        <button type="submit" class="btn btn-success">Edit</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                @endif
                <div class="clearfix"></div>
            </div>


        </div>

    </div>
@endsection
