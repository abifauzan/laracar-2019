<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;
use App\Model\Role;
use App\Model\User;
use App\Model\CarType;

class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Add Role
        Role::create([
            'name' => 'salesman',
            'description' => 'Role of Salesman'
        ]);
        Role::create([
            'name' => 'customer',
            'description' => 'Role of Customer'
        ]);
        User::create([
            'id_role' => 1,
            'name' => 'Agent Smith',
            'email' => 'smith@gmail.com',
            'password' => bcrypt('secret')
        ]);
        User::create([
            'id_role' => 2,
            'name' => 'Abi Fauzan',
            'email' => 'abi@gmail.com',
            'password' => bcrypt('secret')
        ]);
        CarType::create([
            'name' => 'Hatchback',
            'description' => 'Hatchback Car'
        ]);
        CarType::create([
            'name' => 'Coupe',
            'description' => 'Coupe Car'
        ]);
        CarType::create([
            'name' => 'Minivan',
            'description' => 'Minivan Car'
        ]);
        CarType::create([
            'name' => 'SUV',
            'description' => 'SUV Car'
        ]);
        CarType::create([
            'name' => 'Sedan',
            'description' => 'Sedan Car'
        ]);

    }
}
