<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_role')->unsigned();
            $table->string('name', 50);
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('password_resets', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });

        Schema::create('transactions', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('invoice_id')->primary();
            $table->integer('id_customer')->unsigned()->nullable();
            $table->integer('id_salesman')->unsigned()->nullable();
            $table->integer('id_owner')->unsigned()->nullable();
            $table->enum('transaction_type', ['buy', 'sell']);
            $table->string('car_licence');
            $table->string('price');
            $table->string('salesman_commision')->nullable();
            $table->timestamps();
        });

        Schema::create('roles', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name',30);
            $table->string('description',50);
        });

        Schema::create('car_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
        });

        Schema::create('cars', function (Blueprint $table) {
            $table->string('car_licence')->primary();
            $table->integer('id_type')->unsigned();
            $table->integer('id_owner')->unsigned();
            $table->string('year', 4);
            $table->string('brand');
            $table->string('cc');
            $table->string('color');
            $table->string('price');
            $table->enum('status', ['owned', 'sell']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('password_resets');
        Schema::dropIfExists('transactions');
        Schema::dropIfExists('car_types');
        Schema::dropIfExists('cars');
        Schema::dropIfExists('roles');
    }
}
