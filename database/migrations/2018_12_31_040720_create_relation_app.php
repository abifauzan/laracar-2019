<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelationApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // define the foreign key
        Schema::table('users', function($table) {
            $table->foreign('id_role')
                ->references('id')->on('roles')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transactions', function($table) {
            $table->foreign('id_customer')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transactions', function($table) {
            $table->foreign('id_salesman')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transactions', function($table) {
            $table->foreign('id_owner')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('transactions', function($table) {
            $table->foreign('car_licence')
                ->references('car_licence')->on('cars')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('cars', function($table) {
            $table->foreign('id_type')
                ->references('id')->on('car_types')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('cars', function($table) {
            $table->foreign('id_owner')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

    }
}
