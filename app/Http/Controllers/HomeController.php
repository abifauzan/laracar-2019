<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Model\CarType;
use App\Model\Car;
use App\Model\Role;
use App\Model\Transaction;
use App\Model\User;

use Auth;
use DB;
use Alert;
use Validator;

class HomeController extends Controller
{
    public function __construct() {
        return $this->middleware('auth');
    }

    public function index() {
        return view('home');
    }

    public function viewSell() {
        $carType = CarType::all();
        return view('sell', compact('carType'));
        // return $carType;
    }

    public function postSell(Request $request) {
        $validator = Validator::make($request->all(), [
            'car_licence' => 'required',
            'id_type' => 'required',
            'year' => 'required',
            'brand' => 'required',
            'cc' => 'required',
            'color' => 'required',
            'price' => 'required'
        ]);
        if($validator->fails()) {
            Alert::error('Cannot add data.')->persistent('Close');
            return redirect()->back();
        }

        $a = new Car();
        $a->car_licence = strtoupper($request->input('car_licence'));
        $a->id_type = $request->input('id_type');
        $a->id_owner = Auth::user()->id;
        $a->year = $request->input('year');
        $a->brand = $request->input('brand');
        $a->cc = $request->input('cc');
        $a->color = $request->input('color');
        $a->price = $request->input('price');
        $a->status = 'sell';
        $a->save();

        $last = DB::table('transactions')->orderBy('invoice_id', 'desc')->first();

        $b = new Transaction();

        if ($last) {
            $part = $last->invoice_id;
            $b->invoice_id = $part + 1;
        } else {
            $b->invoice_id = rand(1000,9999);
        }
        $b->id_owner = Auth::user()->id;
        $b->transaction_type = 'sell';
        $b->car_licence = $request->input('car_licence');
        $b->price = $request->input('price');
        $b->save();

        if($a && $b) {
            Alert::success('Success add car');
            return redirect()->route('my.car');
        }
        Alert::error('Cannot add car, there\' something\'s wrong with the system');
        return redirect()->back();
    }

    public function paymentHistory() {
        $dataSell = Transaction::where('id_owner', Auth::user()->id)->with('owner')->get();
        $dataBuy = Transaction::where('id_customer', Auth::user()->id)->with('owner', 'salesman')->get();
        return view('payment-history', compact('dataSell', 'dataBuy'));
        // return $dataBuy;
    }

    public function incomingRequest() {
        $datas = Transaction::where([
                    ['transaction_type','=', 'buy'],
                    ['id_owner', Auth::user()->id]
                ])
                ->whereNotNull('id_customer')
                ->with('customer','salesman','owner')
                ->get();
        return view('incoming-request', compact('datas'));
        // return $datas;
    }

    public function myCar() {
        $datas = Car::where('id_owner', Auth::user()->id)->with('owner')->get();
        $carType = CarType::all();
        return view('my-car', compact('datas', 'carType'));
    }

    public function carEdit(Request $request) {

        $data = Car::find($request->input('car_licence'));
        // $data->car_licence = $request->input('car_licence');
        $data->id_type = $request->input('type');
        $data->year = $request->input('year');
        $data->brand = $request->input('brand');
        $data->cc = $request->input('cc');
        $data->cc = $request->input('color');
        $data->price = $request->input('price');
        // return $data;
        $data->save();

        if($data) {
            Alert::success('Success edit data car');
            return redirect()->back();
        }

        Alert::error('Cannot update data');
        return redirect()->back();
    }

    public function carDelete(Request $request) {
        $data = Car::findOrFail($request->car_licence);
        // return $data;
        $data->delete();
        if ($data) {
            Alert::success('Success Delete Data');
            return redirect()->back();
        }
        Alert::error('Cannot delete data');
        return redirect()->back();
    }

    public function carStatusSell(Request $request) {
        $data = Car::findOrFail($request->car_licence);
        $data->status = 'sell';
        $data->save();

        if($data) {
            Alert::success('Success sell car : '. $data->brand);
            return redirect()->back();
        }
        Alert::error('Cannot sell car');
        return redirect()->back();
    }

    public function carStatusUnsell(Request $request) {
        $data = Car::findOrFail($request->car_licence);
        $data->status = 'owned';
        $data->save();

        if($data) {
            Alert::success('Success change status to owned of car : '. $data->brand);
            return redirect()->back();
        }
        Alert::error('Cannot change status car');
        return redirect()->back();
    }

    public function paymentDelete(Request $request) {
        $data = Transaction::findOrFail($request->invoice_id);
        // return $data;
        $data->delete();
        if ($data) {
            Alert::success('Success Delete Data');
            return redirect()->back();
        }
        Alert::error('Cannot delete data');
        return redirect()->back();
    }


}
