<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $primaryKey = 'car_licence';
    public $incrementing = false;
    protected $guarded = [];

    public function owner() {
        return $this->hasOne('App\Model\User', 'id', 'id_owner');
    }

    public function type() {
        return $this->hasOne('App\Model\CarType', 'id', 'id_type');
    }

}
