<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'id_role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role() {
        return $this->hasOne('App\Model\Role', 'id', 'id_role');
    }

    public function customer() {
        return $this->belongsTo('App\Model\Transaction', 'id', 'id_customer');
    }

    public function salesman() {
        return $this->belongsTo('App\Model\Transaction', 'id', 'id_salesman');
    }

    public function owner() {
        return $this->belongsTo('App\Model\Transaction', 'id', 'id_owner');
    }

    public function car() {
        return $this->hasMany('App\Model\Car', 'id_owner', 'id');
    }


}
