<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $primaryKey = 'invoice_id';
    public $incrementing = false;

    protected $guarded = [];

    public function customer() {
        return $this->hasOne('App\Model\User', 'id', 'id_customer');
    }

    public function salesman() {
        return $this->hasOne('App\Model\User', 'id', 'id_salesman');
    }

    public function owner() {
        return $this->hasOne('App\Model\User', 'id', 'id_owner');
    }

    public function cars() {
        return $this->hasMany('App\Model\Car', 'car_licence', 'car_licence');
    }

}
