<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{
    protected $guarded = [];

    public $timestamps = false;
}
